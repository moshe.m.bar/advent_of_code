from functools import reduce

import advent_of_code

tests = {"D2FE28": 6,
         "8A004A801A8002F478": 16,
         "620080001611562C8802118E34": 12,
         "C0015000016115A2E0802F182340": 23,
         "A0016C880162017C3686B18A3D4780": 31}


class Packet:
    def __init__(self, version, type_id, sub_packets, end_idx):
        self.version = version
        self.type_id = type_id
        self.sub_packets = sub_packets
        self.end_idx = end_idx

    def __repr__(self):
        return f"({self.version}, {self.type_id}, {self.sub_packets})"


def get_input(is_test, is_part2=False):
    def _get_input(string):
        return bin(int("F" + string, 16))[2 + 4:]

    if is_test:
        for test, res in (tests2 if is_part2 else tests).items():
            yield _get_input(test), res
    else:
        yield _get_input(advent_of_code.get_input_(16).read().strip())


def parse_literal(value):
    binary = ""
    idx = 0
    read_next = value[idx] == "1"
    binary += value[idx + 1: idx + 4 + 1]
    while read_next and value[idx + 5:]:
        idx += 5
        read_next = value[idx] == "1"
        binary += value[idx + 1: idx + 4 + 1]
    return int(binary, 2), idx + 4 + 1


def parse_operator(operator):
    res = []
    idx = 0
    if not operator:
        return res, idx
    length_type_id = operator[idx]
    if length_type_id == "0":
        # 15 bits length number telling the number of bits in the sub packets
        length_in_bits = int(operator[1: 16], 2)
        idx = 16
        packet = parse(operator[idx:])
        if not packet:
            return res, idx
        idx = idx + packet.end_idx
        res.append(packet)
        while idx < 16 + length_in_bits:
            packet = parse(operator[idx:16 + length_in_bits])
            if not packet:
                return res, idx
            idx = idx + packet.end_idx
            res.append(packet)
    if length_type_id == "1":
        sub_packets_count = int(operator[1: 12], 2)
        idx = 12
        while sub_packets_count:
            packet = parse(operator[idx:])
            if not packet:
                return res, idx
            res.append(packet)
            idx = idx + packet.end_idx
            sub_packets_count -= 1
    return res, idx


def parse(packet):
    if len(packet) < 6:
        return
    version = int(packet[0:3], 2)
    type_id = int(packet[3:6], 2)
    if type_id == 4:
        literal, end_idx = parse_literal(packet[6:])
        return Packet(version, type_id, [literal], end_idx + 6)
    packet, end_idx = parse_operator(packet[6:])
    return Packet(version, type_id, packet, end_idx + 6)


def get_version_sum(packets):
    version_sum = 0
    for packet in packets:
        if packet is None or type(packet) == int:
            continue
        version_sum += packet.version
        version_sum += get_version_sum(packet.sub_packets)
    return version_sum


def part1test():
    test = get_input(True)
    for t, res in test:
        packet = parse(t)
        print(f"{packet.version + get_version_sum(packet.sub_packets)} --- {res}")
        assert packet.version + get_version_sum(packet.sub_packets) == res


def part1():
    packet = parse(next(get_input(False)))
    print(packet)
    print(packet.version + get_version_sum(packet.sub_packets))


"""
C200B40A82 finds the sum of 1 and 2, resulting in the value 3.
04005AC33890 finds the product of 6 and 9, resulting in the value 54.
880086C3E88112 finds the minimum of 7, 8, and 9, resulting in the value 7.
CE00C43D881120 finds the maximum of 7, 8, and 9, resulting in the value 9.
D8005AC2A8F0 produces 1, because 5 is less than 15.
F600BC2D8F produces 0, because 5 is not greater than 15.
9C005AC2F8F0 produces 0, because 5 is not equal to 15.
9C0141080250320F1802104A08 produces 1, because 1 + 3 = 2 * 2.
What do you get if you evaluate the expression represented by your hexadecimal-encoded BITS transmission?"""

OPS = {0: sum,
       1: lambda iterable: reduce(lambda x, y: x * y, iterable),
       2: min,
       3: max,
       5: lambda pair: 1 if pair[0] > pair[1] else 0,
       6: lambda pair: 1 if pair[0] < pair[1] else 0,
       7: lambda pair: 1 if pair[0] == pair[1] else 0}

tests2 = {"C200B40A82": 3,
          "04005AC33890": 54,
          "880086C3E88112": 7,
          "CE00C43D881120": 9,
          "D8005AC2A8F0": 1,
          "F600BC2D8F": 0,
          "9C005AC2F8F0": 0,
          "9C0141080250320F1802104A08": 1}


def get_op_result(packet):
    if packet.type_id == 4:
        return packet.sub_packets[0]
    return OPS[packet.type_id]([get_op_result(sub_packet) for sub_packet in packet.sub_packets])


def part2test():
    for t, res in get_input(is_test=True, is_part2=True):
        packet = parse(t)
        print(f"{get_op_result(packet)} --- {res}")
        assert get_op_result(packet) == res


def part2():
    packet = parse(next(get_input(False)))
    print(packet)
    print(get_op_result(packet))


if __name__ == "__main__":
    part2test()
