import math
import re
from advent_of_code import get_input_

test = """6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5"""


class HashableDict(dict):
    def __hash__(self):
        to_be_frozen = set()
        for k, v in self.items():
            to_be_frozen.add((k, v))
        return hash(frozenset(to_be_frozen))


class Paper:
    def __init__(self):
        self.paper = set()

    def __len__(self):
        return len(self.paper)

    def __repr__(self):
        max_x = -math.inf
        max_y = -math.inf
        dots = set()
        for dot in self.paper:
            max_x = max(max_x, dot['x'])
            max_y = max(max_y, dot['y'])
            dots.add((dot['x'], dot['y']))
        res = []
        for y in range(max_y + 1):
            line = []
            for x in range(max_x + 1):
                line.append(' ' if (x, y) not in dots else '█')
            res.append(line)
        return "\n".join("".join(line) for line in res)

    def read_dot(self, line):
        x, y = line.strip().split(",")
        self.paper.add(HashableDict({'x': int(x), 'y': int(y)}))

    def fold(self, axis_name, axis):
        new_paper = set()
        for dot in self.paper:
            if dot[axis_name] > axis:
                keys = set(dot.keys())
                keys.remove(axis_name)
                other = list(keys)[0]
                new_paper.add(HashableDict({axis_name: axis - (dot[axis_name] - axis), other: dot[other]}))
            else:
                new_paper.add(dot)
        self.paper = new_paper


instructions_re = re.compile(r"fold along ([xy])=(\d+)")


def read_instructions(instructions, line):
    match = instructions_re.match(line)
    instructions.append((match.group(1), int(match.group(2))))


def get_input(is_test):
    paper = Paper()
    instructions = []
    dots = True
    for line in test.splitlines() if is_test else get_input_(13):
        line = line.strip()
        if line == "":
            dots = False
            continue
        if dots:
            paper.read_dot(line)
        else:
            read_instructions(instructions, line)
    return paper, instructions


def part(is_test=False):
    paper, instructions = get_input(is_test)
    for instruction in instructions:
        axis_name, axis = instruction
        paper.fold(axis_name, axis)
    # for part 1 place the print statement in the loop and break
    print(paper)
    print(len(paper))


if __name__ == "__main__":
    part()
