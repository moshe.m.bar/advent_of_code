import advent_of_code.advent_of_code_utils as advent_of_code_utils

test = """[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"""

scores = {')': 3,
          ']': 57,
          '}': 1197,
          '>': 25137}

scores2 = {')': 1,
           ']': 2,
           '}': 3,
           '>': 4}

matcher = {')': '(',
           ']': '[',
           '}': '{',
           '>': '<'}

matcher2 = {'(': ')',
            '[': ']',
            '{': '}',
            '<': '>'}


def get_input(is_test=False):
    return map(list, test.splitlines()) if is_test else map(lambda l: list(l.strip()), advent_of_code_utils.get_input_(10).readlines())


def part1(is_test=False):
    score = 0
    for line in get_input(is_test):
        stack = []
        for c in line:
            if c in matcher.values():
                stack.append(c)
            elif matcher[c] == stack[-1]:
                stack.pop()
            else:
                score += scores[c]
                break
    print(score)


def new_score(score, c):
    return 5 * score + scores2[c]


def part2(is_test=False):
    scores = []
    for line in get_input(is_test):
        stack = []
        corrupt = False
        score = 0
        for c in line:
            if c in matcher.values():
                stack.append(c)
            elif matcher[c] == stack[-1]:
                stack.pop()
            else:
                corrupt = True
                break
        if corrupt:
            continue
        for c in stack[::-1]:
            score = new_score(score, matcher2[c])
        scores.append(score)
    scores = sorted(scores)
    print(scores[len(scores) // 2])


if __name__ == "__main__":
    part1()
    part2()