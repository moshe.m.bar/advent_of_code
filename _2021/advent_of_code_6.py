from collections import defaultdict

import advent_of_code.advent_of_code_utils as advent_of_code_utils

test = "3,4,3,1,2"


def get_input(is_test=False):
    input = test if is_test else advent_of_code_utils.get_input_(6).readline()
    school = defaultdict(int)
    for i in input.strip().split(","):
        i = int(i)
        school[i] += 1
    return school


def next_day(school):
    """Each day, a 0 becomes a 6 and adds a new 8 to the end of the list,
        while each other number decreases by 1 if it was present at the start of the day."""
    tomorrow_school = defaultdict(int)
    for i in range(8):
        tomorrow_school[i] = school[i + 1]
    tomorrow_school[6] += school[0]
    tomorrow_school[8] += school[0]
    return tomorrow_school


def total(school):
    return sum(school.values())


def total_after_n(school, n):
    while n > 0:
        school = next_day(school)
        n -= 1
    return total(school)


if __name__ == "__main__":
    print(total_after_n(get_input(), 80))
    print(total_after_n(get_input(), 256))
