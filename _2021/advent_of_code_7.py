import advent_of_code.advent_of_code_utils as advent_of_code_utils

test = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]


def get_input(is_test=False):
    if is_test:
        return test
    return list(map(int, advent_of_code_utils.get_input_(7).readline().split(",")))


def part1(is_test=False):
    sorted_input = sorted(get_input(is_test))
    goal = sorted_input[len(sorted_input) // 2]
    cost = 0
    for p in sorted_input:
        cost += abs(p - goal)
    print(cost)


def part2(is_test=False):
    input_ = get_input(is_test)
    goal = (sum(input_) // len(input_))
    cost = 0
    for p in input_:
        cost += cost2(p, goal)
    print(cost)


def cost2(p, goal):
    """Sn = n * (a_1 + a_n) / 2"""
    n = abs(p - goal)
    return (n * (1 + n)) // 2


if __name__ == "__main__":
    part1()
    part2()
