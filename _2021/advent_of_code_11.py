from advent_of_code import valid_in_matrix, get_input_

test = """5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526"""


class Cell:
    def __init__(self, val):
        self.val = val
        self.flashed = False

    def __int__(self):
        return self.val

    def __add__(self, other):
        return Cell(self.val + int(other))

    def __radd__(self, other):
        return other + self.val

    def __gt__(self, other):
        return self.val > int(other)

    def __eq__(self, other):
        return self.val == int(other)

    def __repr__(self):
        return f"{self.val}|{'*' if self.flashed else ' '}"


def get_input(is_test=False):
    input_ = test.splitlines() if is_test else get_input_(11)
    octopuses = []
    for line in input_:
        row = []
        for o in list(line.strip()):
            row.append(Cell(int(o)))
        octopuses.append(row)
    return octopuses


def step_if_not_flashed(i, j, octopuses):
    if valid_in_matrix(i, j, octopuses) and not octopuses[i][j].flashed:
        step_aux(i, j, octopuses)


def step_aux(i, j, octopuses):
    octopuses[i][j].val += 1
    if octopuses[i][j].val > 9 and not octopuses[i][j].flashed:
        octopuses[i][j].flashed = True
        # check up
        i_, j_ = i - 1, j
        step_if_not_flashed(i_, j_, octopuses)
        # check up-left
        i_, j_ = i - 1, j - 1
        step_if_not_flashed(i_, j_, octopuses)
        # check up-right
        i_, j_ = i - 1, j + 1
        step_if_not_flashed(i_, j_, octopuses)
        # check down
        i_, j_ = i + 1, j
        step_if_not_flashed(i_, j_, octopuses)
        # check down-left
        i_, j_ = i + 1, j - 1
        step_if_not_flashed(i_, j_, octopuses)
        # check down-right
        i_, j_ = i + 1, j + 1
        step_if_not_flashed(i_, j_, octopuses)
        # check left
        i_, j_ = i, j - 1
        step_if_not_flashed(i_, j_, octopuses)
        # check right
        i_, j_ = i, j + 1
        step_if_not_flashed(i_, j_, octopuses)


def step(octopuses):
    """
    - First, the energy level of each octopus increases by 1.

    - Then, any octopus with an energy level greater than 9 flashes.
      This increases the energy level of all adjacent octopuses by 1,
      including octopuses that are diagonally adjacent.
      If this causes an octopus to have an energy level greater than 9, it also flashes.
      This process continues as long as new octopuses keep having their energy level increased beyond 9.
      (An octopus can only flash at most once per step.)

    - Finally, any octopus that flashed during this step has its energy level set to 0,
      as it used all of its energy to flash.
    """
    for i in range(len(octopuses)):
        for j in range(len(octopuses[i])):
            step_aux(i, j, octopuses)
    flashes = 0
    for row in octopuses:
        for o in row:
            if o.flashed:
                flashes += 1
                o.flashed = False
                o.val = 0
    return flashes


def part1(n, is_test=False):
    octopuses = get_input(is_test)
    flashes = 0
    for i in range(n):
        flashes += step(octopuses)
    print(flashes)


def part2(is_test=False):
    octopuses = get_input(is_test)
    flashes = 0
    i = 0
    while flashes != 100:
        i += 1
        flashes = step(octopuses)
        if flashes == 100:
            print(i)
            break
        flashes = 0


if __name__ == "__main__":
    part1(100)
    part2()