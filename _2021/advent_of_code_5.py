from collections import defaultdict

import advent_of_code.advent_of_code_utils as advent_of_code_utils

f = advent_of_code_utils.get_input_(5)
test = """0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"""


def get_lines(is_part1=True):
    lines = []
    f.seek(0)
    for line in f:
    # for line in test.splitlines():
        pair1, pair2 = line.strip().split(" -> ")
        x1, y1 = pair1.strip().split(",")
        x2, y2 = pair2.strip().split(",")
        if is_part1 and x1 != x2 and y1 != y2:
            continue
        lines.append((int(x1), int(y1), int(x2), int(y2)))
    return lines


def part1():
    lines_map = defaultdict(lambda: defaultdict(int))
    for line in get_lines():
        x1, y1, x2, y2 = line
        if x1 == x2:
            for y in range(min(y1, y2), max(y1, y2) + 1):
                lines_map[x1][y] += 1
        if y1 == y2:
            for x in range(min(x1, x2), max(x1, x2) + 1):
                lines_map[x][y1] += 1
    print_bigger_than_2(lines_map)


def print_bigger_than_2(lines_map):
    count = 0
    for x in lines_map.keys():
        for y in lines_map[x].keys():
            if lines_map[x][y] >= 2:
                count += 1
    print(count)


def part2():
    lines_map = defaultdict(lambda: defaultdict(int))
    for line in get_lines(is_part1=False):
        x1, y1, x2, y2 = line
        x_sign = 1 if x1 <= x2 else -1
        y_sign = 1 if y1 <= y2 else -1
        did_last = False
        while (x1 != x2 or y1 != y2) or not did_last:
            if x1 == x2 and y1 == y2:
                did_last = True
            lines_map[x1][y1] += 1
            if not did_last:
                if x1 == x2:
                    y1 += y_sign
                elif y1 == y2:
                    x1 += x_sign
                else:
                    x1 += x_sign
                    y1 += y_sign
    print_bigger_than_2(lines_map)


if __name__ == "__main__":
    part1()
    part2()
