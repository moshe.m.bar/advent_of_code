import advent_of_code
from collections import defaultdict

test = """NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"""


def get_template(line):
    return list(line.strip())


def get_rule(line):
    line = line.strip()
    return line.split(" -> ")


def get_input(is_test):
    rules = {}
    input_ = (line for line in (test.splitlines() if is_test else advent_of_code.get_input_(14)))
    template = get_template(next(input_))
    next(input_)
    for line in input_:
        pair, element = get_rule(line)
        rules[pair] = element
    return template, rules


def get_counts(polymer):
    counts = defaultdict(int)
    for element in polymer:
        counts[element] += 1
    return counts


def most_common_minus_least_common(counts):
    """most common element and subtract the quantity of the least common"""
    return max(counts.items(), key=lambda pair: pair[1])[1] - min(counts.items(), key=lambda pair: pair[1])[1]


def part1(n, is_test=False):
    template, rules = get_input(is_test)
    for step in range(n):
        previous = template[0]
        polymer = [previous]
        for element in template[1:]:
            polymer.append(rules[f"{previous}{element}"])
            polymer.append(element)
            previous = element
        template = polymer
    counts = get_counts(template)
    print(most_common_minus_least_common(counts))


def part2(n, is_test=False):
    """same as part1, but optimized"""
    template, rules = get_input(is_test)
    pairs = defaultdict(int)
    counts = defaultdict(int)
    previous = template[0]
    counts[previous] += 1
    for element in template[1:]:
        pairs[f"{previous}{element}"] += 1
        counts[element] += 1
        previous = element
    for step in range(n):
        new_pairs = defaultdict(int)
        for pair, count in pairs.items():
            element1, element2 = list(pair)
            new_element = rules[f"{element1}{element2}"]
            counts[new_element] += count
            new_pairs[f"{element1}{new_element}"] += count
            new_pairs[f"{new_element}{element2}"] += count
        pairs = new_pairs
    print(most_common_minus_least_common(counts))


if __name__ == "__main__":
    part1(10, True)
    part2(40, True)
    part1(10)
    part2(40)
