import math

draws = []
boards = []
BOARD_SIZE = 5

f = open("/Users/moshebar-mbpr16/Downloads/advent_of_code/input4.txt")


class Cell:
    def __init__(self, val):
        self.val = int(val)
        self.marked = False

    def __repr__(self):
        return f"{self.val}|{'*' if self.marked else ' '}"


class Board:
    def __init__(self):
        self.b = []
        self.won = False

    def add(self, line):
        if len(self.b) >= BOARD_SIZE:
            raise Exception("board is full")
        self.b.append([Cell(val) for val in line.strip().split()])

    def mark(self, val):
        for i in range(BOARD_SIZE):
            for j in range(BOARD_SIZE):
                if self.b[i][j].val == val:
                    self.b[i][j].marked = True
                    self.check_win(i, j)

    def check_win(self, i, j):
        if all(self.b[i][k].marked for k in range(BOARD_SIZE)) or all(self.b[k][j].marked for k in range(BOARD_SIZE)):
            self.won = True
        return self.won

    def score(self, last_draw):
        if not self.won:
            return -math.inf
        sum_ = 0
        for i in range(BOARD_SIZE):
            for j in range(BOARD_SIZE):
                if not self.b[i][j].marked:
                    sum_ += self.b[i][j].val
        return sum_ * last_draw

    def __repr__(self):
        return str(self.b)


def read_draws():
    f.seek(0)
    global draws
    draws = [int(draw) for draw in f.readline().strip().split(",")]


def read_board():
    board = Board()
    for i in range(BOARD_SIZE):
        board.add(f.readline())
    return board


def read_boards():
    global boards
    boards = []
    next_line = f.readline()
    while next_line == f.newlines:
        boards.append(read_board())
        next_line = f.readline()


def solution():
    read_draws()
    read_boards()
    global boards
    score = None
    for draw in draws:
        for board in boards:
            board.mark(draw)
            if board.won:
                score = board.score(draw)
            boards = [b for b in boards if not b.won]
    print(score)


if __name__ == "__main__":
    solution()
