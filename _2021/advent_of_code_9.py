from functools import reduce
import advent_of_code.advent_of_code_utils as advent_of_code_utils
from advent_of_code import valid

test = """2199943210
3987894921
9856789892
8767896789
9899965678"""


class Cell:
    def __init__(self, val):
        self.val = val
        self.visited = False

    def __int__(self):
        return self.val

    def __add__(self, other):
        return Cell(self.val + int(other))

    def __radd__(self, other):
        return other + self.val

    def __gt__(self, other):
        return self.val > int(other)

    def __eq__(self, other):
        return self.val == int(other)

    def __repr__(self):
        return f"{self.val}|{'*' if self.visited else ' '}"


def get_input(is_test=False):
    res = []
    for line in test.splitlines() if is_test else advent_of_code_utils.get_input_(9):
        res.append(list(map(lambda s: Cell(int(s)), list(line.strip()))))
    return res


def is_low(i, j, height_map):
    low = True
    # check up
    if valid(i - 1, height_map) and valid(j, height_map[i - 1]):
        low = low and height_map[i - 1][j] > height_map[i][j]
    # check down
    if valid(i + 1, height_map) and valid(j, height_map[i + 1]):
        low = low and height_map[i + 1][j] > height_map[i][j]
    # check left
    if valid(j - 1, height_map[i]):
        low = low and height_map[i][j - 1] > height_map[i][j]
    # check right
    if valid(j + 1, height_map[i]):
        low = low and height_map[i][j + 1] > height_map[i][j]
    return low


def part1():
    risk_sum = 0
    height_map = get_input()
    for i in range(len(height_map)):
        for j in range(len(height_map[i])):
            if is_low(i, j, height_map):
                risk_sum += 1 + height_map[i][j]
    print(risk_sum)


def basin_size_aux(i, j, height_map, size):
    # check up
    height_map[i][j].visited = True
    if valid(i - 1, height_map) and valid(j, height_map[i - 1]) and not height_map[i - 1][j].visited:
        size = basin_size_aux(i - 1, j, height_map, size + 1) if 9 != height_map[i - 1][j] > height_map[i][j] else size
    # check down
    if valid(i + 1, height_map) and valid(j, height_map[i + 1]) and not height_map[i + 1][j].visited:
        size = basin_size_aux(i + 1, j, height_map, size + 1) if 9 != height_map[i + 1][j] > height_map[i][j] else size
    # check left
    if valid(j - 1, height_map[i]) and not height_map[i][j - 1].visited:
        size = basin_size_aux(i, j - 1, height_map, size + 1) if 9 != height_map[i][j - 1] > height_map[i][j] else size
    # check right
    if valid(j + 1, height_map[i]) and not height_map[i][j + 1].visited:
        size = basin_size_aux(i, j + 1, height_map, size + 1) if 9 != height_map[i][j + 1] > height_map[i][j] else size
    return size


def basin_size(i_low, j_low, height_map):
    return basin_size_aux(i_low, j_low, height_map, 1)


def part2(is_test=False):
    basins = []
    height_map = get_input(is_test)
    for i in range(len(height_map)):
        for j in range(len(height_map[i])):
            if is_low(i, j, height_map):
                # find basin size
                basins.append(basin_size(i, j, height_map))
    basins = sorted(basins, reverse=True)
    print(reduce(lambda x, y: x * y, basins[:3]))


if __name__ == "__main__":
    part1()
    part2()
