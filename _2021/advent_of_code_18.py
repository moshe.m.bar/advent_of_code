import typing
from copy import deepcopy
from functools import reduce
from math import ceil, floor, inf
import advent_of_code

test = """[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"""


def get_input():
    res = []
    with advent_of_code.get_input_(18) as f:
        for line in f:
            res.append(SFNumber.from_string(line.strip()))
    return res


def is_int(i):
    return type(i) == int


def splitable(x):
    return is_int(x) and x >= 10


class SFNumber:
    @staticmethod
    def from_string(string):
        assert string.startswith("[") and string.endswith("]")
        string = string[1:-1]
        current_comma_idx_validator = 0
        current_comma_idx = None
        for i in range(len(string)):
            if string[i] == "[":
                current_comma_idx_validator += 1
            elif string[i] == "]":
                current_comma_idx_validator -= 1
            elif string[i] == "," and current_comma_idx_validator == 0:
                current_comma_idx = i
                break
        left = string[:current_comma_idx]
        right = string[current_comma_idx + 1:]
        if "[" in left:
            left = SFNumber.from_string(left)
        else:
            left = int(left)
        if "[" in right:
            right = SFNumber.from_string(right)
        else:
            right = int(right)
        return SFNumber(left, right)

    def __init__(self,
                 left: typing.Union[int, "SFNumber"],
                 right: typing.Union[int, "SFNumber"],
                 _deepcopy=False,
                 exploding_depth=4):
        self.left = deepcopy(left) if _deepcopy else left
        self.right = deepcopy(right) if _deepcopy else right
        l_depth = 0 if is_int(left) else left.depth
        r_depth = 0 if is_int(right) else right.depth
        self.depth = max(l_depth, r_depth) + 1
        self.eploding_depth = exploding_depth

    def __deepcopy__(self, memo):
        return SFNumber(deepcopy(self.left), deepcopy(self.right))

    def __add__(self, other):
        res = SFNumber(self, other, _deepcopy=True)
        res.reduce()
        return res

    def reduce(self):
        while self.explode():
            continue
        if self.split():
            self.reduce()

    def explode(self):
        exploded, _, _ = self.inorder_explode(0)
        return exploded

    def split(self):
        return self.inorder_split()

    def magnitude(self):
        left_magnitude = self.left if is_int(self.left) else self.left.magnitude()
        right_magnitude = self.right if is_int(self.right) else self.right.magnitude()
        return 3 * left_magnitude + 2 * right_magnitude

    def inorder_split(self):
        if splitable(self.left):  # we are doing the leftmost split
            self.left = SFNumber(floor(self.left / 2), ceil(self.left / 2))
            return True
        splited_below = False
        if not is_int(self.left):  # keep looking for a split in order
            splited_below = self.left.inorder_split()
        pass  # TODO: nothing to do with the current node, can only split ints
        if not splited_below and splitable(self.right):  # we are the leftmost split
            self.right = SFNumber(floor(self.right / 2), ceil(self.right / 2))
            return True
        if not splited_below and not is_int(self.right):  # keep looking for a split in order
            splited_below = self.right.inorder_split()
        return splited_below

    def _handle_explosion_right_aux(self, val):
        if is_int(self.left):
            self.left += val
        else:
            self.left._handle_explosion_right_aux(val)

    def handle_explosion_right(self, val):
        if is_int(self.right):
            self.right += val
        else:
            self.right._handle_explosion_right_aux(val)

    def _handle_explosion_left_aux(self, val):
        if is_int(self.right):
            self.right += val
        else:
            self.right._handle_explosion_left_aux(val)

    def handle_explosion_left(self, val):
        if is_int(self.left):
            self.left += val
        else:
            self.left._handle_explosion_left_aux(val)

    def inorder_explode(self, depth):
        exploded, add_left, add_right = False, None, None
        if not is_int(self.left):  # keep looking for explosion to the left
            exploded, add_left, add_right = self.left.inorder_explode(depth + 1)
        if exploded:
            if add_right is not None:
                if add_left is not None:  # we are right after the explosion, replace son with 0
                    self.left = 0
                self.handle_explosion_right(add_right)
                return exploded, add_left, None
            return exploded, add_left, add_right
        if not exploded and depth == self.eploding_depth and self.both_ints():  # we are the leftmost explosion
            return True, self.left, self.right
        if not exploded and not is_int(self.right):
            exploded, add_left, add_right = self.right.inorder_explode(depth + 1)
        if exploded:
            if add_left is not None:
                if add_right is not None:  # we are right after the explosion, replace son with 0
                    self.right = 0
                self.handle_explosion_left(add_left)
                return exploded, None, add_right
            return exploded, add_left, add_right
        return exploded, add_left, add_right

    def both_ints(self):
        return is_int(self.left) and is_int(self.right)

    def __repr__(self):
        return f"[{self.left},{self.right}]"


def tests():
    def test_explode(orig, result):
        sf_n = SFNumber.from_string(orig)
        sf_n.explode()
        assert str(sf_n) == result

    def test_sum(sf_list, result):
        sf_list = sf_list.splitlines()
        sum_ = SFNumber.from_string(sf_list[0])
        for sf_string in sf_list[1:]:
            sum_ = sum_ + SFNumber.from_string(sf_string)
        assert str(sum_) == result
        return sum_

    def test_magnitude(sf_string, mag):
        assert SFNumber.from_string(sf_string).magnitude() == mag

    assert str(SFNumber.from_string("[1,2]") + SFNumber.from_string("[[3,4],5]")) == str(
        SFNumber.from_string("[[1,2],[[3,4],5]]"))

    # test explode ###
    test_explode("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]")
    test_explode("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]")
    test_explode("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]")
    test_explode("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]")
    test_explode("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]")

    # test reduce ###
    assert str(SFNumber.from_string("[[[[4,3],4],4],[7,[[8,4],9]]]") + SFNumber.from_string("[1,1]")) == "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"

    # test sums ###
    tl = """[1,1]
[2,2]
[3,3]
[4,4]"""
    test_sum(tl, "[[[[1,1],[2,2]],[3,3]],[4,4]]")
    tl = """[1,1]
[2,2]
[3,3]
[4,4]
[5,5]"""
    test_sum(tl, "[[[[3,0],[5,3]],[4,4]],[5,5]]")
    tl = """[1,1]
[2,2]
[3,3]
[4,4]
[5,5]
[6,6]"""
    test_sum(tl, "[[[[5,0],[7,4]],[5,5]],[6,6]]")
    tl = """[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]"""
    test_sum(tl, "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")

    # test magnitude ###
    test_magnitude("[[1,2],[[3,4],5]]", 143)
    test_magnitude("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", 1384)
    test_magnitude("[[[[1,1],[2,2]],[3,3]],[4,4]]", 445)
    test_magnitude("[[[[3,0],[5,3]],[4,4]],[5,5]]", 791)
    test_magnitude("[[[[5,0],[7,4]],[5,5]],[6,6]]", 1137)
    test_magnitude("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", 3488)

    final_test = """[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"""
    sum_ = test_sum(final_test, "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]")
    test_magnitude(str(sum_), 4140)

    # test part2
    sf_numbers = list(map(SFNumber.from_string, """[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]""".splitlines()))
    assert part2(sf_numbers) == 3993


def part1(sf_numbers):
    sum_ = reduce(lambda x, y: x + y, sf_numbers)
    return sum_.magnitude()


def part2(sf_numbers):
    max_mag = -inf
    for i in range(len(sf_numbers)):
        for j in range(i + 1, len(sf_numbers)):
            mag1 = (sf_numbers[i] + sf_numbers[j]).magnitude()
            mag2 = (sf_numbers[j] + sf_numbers[i]).magnitude()
            max_mag = max(max_mag, mag1, mag2)
    return max_mag


if __name__ == "__main__":
    tests()
    sf_numbers = get_input()
    print(part1(sf_numbers))
    print(part2(sf_numbers))


