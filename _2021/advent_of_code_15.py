import timeit
from collections import defaultdict
from math import inf

import advent_of_code

test = """1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581"""


def get_input(is_test, to_cells=False):
    map_ = []
    for i, line in enumerate(test.splitlines() if is_test else advent_of_code.get_input_(15)):
        line = line.strip()
        row = []
        for j, c in enumerate(list(line)):
            row.append(Cell(i, j, int(c)) if to_cells else int(c))
        map_.append(row)
    return map_


class Cell:
    def __init__(self, i, j, val):
        self.i = i
        self.j = j
        self.val = val
        self.risk = inf

    def __gt__(self, other):
        return int(self) > int(other)

    def __int__(self):
        return self.risk

    def __eq__(self, other):
        return int(self) == int(other)

    def __le__(self, other):
        return int(self) <= int(other)

    def __lt__(self, other):
        return int(self) < int(other)

    def __ge__(self, other):
        return int(self) >= int(other)

    def __hash__(self):
        return hash((self.i, self.j, self.val))

    def __repr__(self):
        return f"({self.i},{self.j})|{self.val}"

    def __iter__(self):
        yield self.i
        yield self.j

    def same(self, other):
        return self.i == other.i and self.j == other.j


def traverse_neighbors(u, map_, op):
    i, j = u
    i_, j_ = i - 1, j  # up
    if advent_of_code.valid_in_matrix(i_, j_, map_):
        op(i_, j_, u)
    i_, j_ = i + 1, j  # down
    if advent_of_code.valid_in_matrix(i_, j_, map_):
        op(i_, j_, u)
    i_, j_ = i, j - 1  # left
    if advent_of_code.valid_in_matrix(i_, j_, map_):
        op(i_, j_, u)
    i_, j_ = i, j + 1  # right
    if advent_of_code.valid_in_matrix(i_, j_, map_):
        op(i_, j_, u)


def dijkstra(map_):
    def handle_neighbor(risk_u, neighbor_i, neighbor_j):
        curr_risk = risk_u + map_[neighbor_i][neighbor_j]
        if curr_risk < risks[(neighbor_i, neighbor_j)]:
            return curr_risk
        return risks[(neighbor_i, neighbor_j)]

    q = set()
    for i in range(len(map_)):
        for j in range(len(map_[i])):
            q.add((i, j))
    risks = defaultdict(lambda: inf)
    risks[(0, 0)] = 0

    def update_risk(k, t, e):
        risks[(k, t)] = handle_neighbor(risks[e], k, t)

    while q:
        u = min(q, key=lambda v: risks[v])
        q.remove(u)
        traverse_neighbors(u, map_, update_risk)
    return risks


def dijkstra_opt(map_):
    start = map_[0][0]
    start.risk = 0
    q = advent_of_code.HashHeap({start, })
    visited = set()

    def update_risk(k, t, u):
        neighbor = map_[k][t]
        curr_risk = u.risk + neighbor.val
        if curr_risk < neighbor.risk:
            neighbor.risk = curr_risk
        if neighbor not in q:
            q.push(neighbor)

    while q:
        u = q.pop()
        if u in visited:
            continue
        visited.add(u)
        if u.same(map_[-1][-1]):
            return u.risk
        traverse_neighbors(u, map_, update_risk)
    return None


def part1(is_test=False):
    map_ = get_input(is_test)
    print(dijkstra(map_)[(len(map_) - 1, len(map_[-1]) - 1)])


def inc(c, times):
    for _ in range(times):
        c.val = c.val + 1 if c.val < 9 else 1
    return c


def expand(map_, n):
    new_map = []
    for i in range(n):
        for row in map_:
            row = [inc(Cell(c.i, c.j, c.val), i) for c in row]
            new_map.append([])
            for j in range(n):
                new_map[-1].extend([inc(Cell(c.i + i * len(map_), c.j + j * len(map_[i]), c.val), j) for c in row])
    return new_map


def part1_opt(is_test=False):
    map_ = get_input(is_test, to_cells=True)
    print(dijkstra_opt(map_))


def part2(is_test=False):   # very bad, took 9367.108037155 seconds |;
    map_ = expand(get_input(is_test), 5)
    print(dijkstra(map_)[(len(map_) - 1, len(map_[-1]) - 1)])


def part2_opt(is_test=False):
    map_ = expand(get_input(is_test, to_cells=True), 5)
    print(dijkstra_opt(map_))


if __name__ == "__main__":
    print(timeit.timeit(lambda: part1_opt(), number=1))
    print(timeit.timeit(lambda: part2_opt(), number=1))

