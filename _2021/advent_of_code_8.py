import advent_of_code.advent_of_code_utils as advent_of_code_utils

test = """be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"""

"""
  0:      1:      2:      3:      4:
 aaaa    ....    aaaa    aaaa    ....
b    c  .    c  .    c  .    c  b    c
b    c  .    c  .    c  .    c  b    c
 ....    ....    dddd    dddd    dddd
e    f  .    f  e    .  .    f  .    f
e    f  .    f  e    .  .    f  .    f
 gggg    ....    gggg    gggg    ....

  5:      6:      7:      8:      9:
 aaaa    aaaa    aaaa    aaaa    aaaa
b    .  b    .  .    c  b    c  b    c
b    .  b    .  .    c  b    c  b    c
 dddd    dddd    ....    dddd    dddd
.    f  e    f  .    f  e    f  .    f
.    f  e    f  .    f  e    f  .    f
 gggg    gggg    ....    gggg    gggg
 """


def get_input(is_test=False):
    input_ = test.splitlines() if is_test else advent_of_code_utils.get_input_(8)
    return (line.strip().split(" | ") for line in input_)


def part1(is_test=False):
    count = 0
    for signal_pattern, four_digit_value in get_input(is_test):
        for digit in four_digit_value.split():
            if len(digit) == 2 or len(digit) == 4 or len(digit) == 3 or len(digit) == 7:
                count += 1
    print(count)


"""
 0000
1    2
1    2
 3333
4    5
4    5
 6666
 """
segments = list(range(7))

"""
  0(6):  1(2):   2(5):   3(5):  4(4):  
 0000    ....    0000    0000    .... 
1    2  .    2  .    2  .    2  1    2
1    2  .    2  .    2  .    2  1    2
 ....    ....    3333    3333    3333 
4    5  .    5  4    .  .    5  .    5
4    5  .    5  4    .  .    5  .    5
 6666    ....    6666    6666    .... 

  5(5):  6(6):   7(3):  8(7):   9(6):
 0000    0000    0000    0000    0000
1    .  1    .  .    2  1    2  1    2
1    .  1    .  .    2  1    2  1    2
 3333    3333    ....    3333    3333
.    5  4    5  .    5  4    5  .    5
.    5  4    5  .    5  4    5  .    5
 6666    6666    ....    6666    6666
 """
"""
   1:      4:      7:      8:  
  ....    ....    0000    0000 
 .    2  1    2  .    2  1    2
 .    2  1    2  .    2  1    2
  ....    3333    ....    3333 
 .    5  .    5  .    5  4    5
 .    5  .    5  .    5  4    5
  ....    ....    ....    6666 
"""
"""
  2(5):   3(5):    5(5):
  0000    0000    0000  
 .    2  .    2  1    . 
 .    2  .    2  1    . 
  3333    3333    3333  
 4    .  .    5  .    5 
 4    .  .    5  .    5 
  6666    6666    6666  

"""
"""
  0(6):   6(6):   9(6):    
 0000     0000     0000   
1    2   1    .   1    2  
1    2   1    .   1    2  
 ....     3333     3333   
4    5   4    5   .    5  
4    5   4    5   .    5  
 6666     6666     6666
"""
signal_to_digit = {
    frozenset({0, 1, 2, 4, 5, 6}): 0,
    frozenset({2, 5}): 1,
    frozenset({0, 2, 3, 4, 6}): 2,
    frozenset({0, 2, 3, 5, 6}): 3,
    frozenset({1, 2, 3, 5}): 4,
    frozenset({0, 1, 3, 5, 6}): 5,
    frozenset({0, 1, 3, 4, 5, 6}): 6,
    frozenset({0, 2, 5}): 7,
    frozenset({0, 1, 2, 3, 4, 5, 6}): 8,
    frozenset({0, 1, 2, 3, 5, 6}): 9,
}


def find_segment_0(pattern, signals, two_and_five):
    if len(two_and_five):
        for p in list(pattern):
            if p not in two_and_five:
                signals[p] = 0


def signals_to_segment_map(signal_pattern):
    signals = {}
    two_and_five = set()
    one_and_three = set()
    digit0 = None
    digit1 = None
    digit2 = None
    digit3 = None
    digit4 = None
    digit5 = None
    digit6 = None
    digit7 = None
    digit8 = None
    digit9 = None
    # 7 + 1 -> segment0 (not in 1 but in 7) (which is in all but 1 and 4)
    # 1         -> segment2 & 5 approximate location (which both is in 0,1,3,4,7,8,9)
    # 1 + 4     -> segment1 & 3 approximate location (in 4 but not in 1)
    # length 5 with both 1 & 3 == 5
    # length 6 with not both 1 & 3 == 0
    # 0 + 5 + 1 -> segment2 & segment4 (in 1 & 0 but not in 5 is segment2), (in 0 but not in 5 and not segment2 is segment4
    # 2
    while len(signals) < 7:
        for pattern in signal_pattern.split(" "):
            if len(pattern) == 2:
                # digit 1, mark 2 & 5
                two_and_five.update(list(pattern))
                digit1 = frozenset(list(pattern))
            elif len(pattern) == 4:
                # digit 4, mark 1 & 3
                digit4 = frozenset(list(pattern))
                if len(two_and_five):
                    for p in list(pattern):
                        if p not in two_and_five:
                            one_and_three.add(p)
            elif len(pattern) == 3:
                # digit 7, mark 0
                find_segment_0(pattern, signals, two_and_five)
                digit7 = frozenset(list(pattern))
            elif len(pattern) == 7:
                # digit 8, mark?
                digit8 = frozenset(list(pattern))
            elif len(pattern) == 5:
                # digit 2 or 3 or 5
                if len(one_and_three):
                    if all((d in pattern for d in one_and_three)):
                        digit5 = frozenset(list(pattern))
                if 4 in signals.values():
                    for p in pattern:
                        if signals.get(p, -1) == 4:
                            digit2 = frozenset(list(pattern))
            elif len(pattern) == 6:
                # digit 0 or 6 or 9
                if len(one_and_three):
                    if any((d not in pattern for d in one_and_three)):
                        digit0 = frozenset(list(pattern))
            if digit0 and digit1 and digit5:
                for p in digit1:
                    if p in digit0 and p not in digit5:
                        signals[p] = 2
            if digit0 and digit5 and 2 in signals.values():
                for p in digit0:
                    if p not in digit5 and p not in signals:
                        signals[p] = 4
            if 5 not in signals.values():
                if 2 in signals.values() and digit1:
                    for p in digit1:
                        if p not in signals:
                            signals[p] = 5
                            break
            if digit2 and digit4 and 2 in signals.values():
                for p in digit4:
                    if p in digit2 and signals.get(p, -1) != 2:
                        signals[p] = 3
            if digit2 and 0 in signals.values() and 2 in signals.values() and 3 in signals.values() and 4 in signals.values():
                for p in digit2:
                    if signals.get(p, -1) not in {0, 2, 3, 4}:
                        signals[p] = 6
                        break
            if digit5 and 0 in signals.values() and 3 in signals.values() and 5 in signals.values() and 6 in signals.values():
                for p in digit5:
                    if signals.get(p, -1) not in {0, 3, 5, 6}:
                        signals[p] = 1
    return signals


def part2(is_test=False):
    count = 0
    for signal_pattern, four_digit_value in get_input(is_test):
        signals = signals_to_segment_map(signal_pattern)
        for i, digit in enumerate(four_digit_value.split()):
            power = 3 - i
            count += signal_to_digit[frozenset(map(lambda p: signals[p], digit))] * 10**power
    print(count)


if __name__ == "__main__":
    part1()
    part2()
