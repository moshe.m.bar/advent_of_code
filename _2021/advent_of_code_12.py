from advent_of_code import get_input_
from collections import defaultdict

test = """start-A
start-b
A-c
A-b
b-d
A-end
b-end"""

test1 = """dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc"""

test2 = """fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW"""


def get_input(is_test):
    graph = defaultdict(set)
    for line in test2.splitlines() if is_test else get_input_(12):
        line = line.strip()
        left, right = line.split("-")
        graph[left].add(right)
        graph[right].add(left)
    return graph


def find_ways(graph, start, visited):
    if start == 'end':
        return 1
    visited = set(visited)
    visited.add(start)
    ways = 0
    for v in graph[start]:
        if v not in visited or v.isupper():
            ways += find_ways(graph, v, frozenset(visited))
    return ways


def part1(is_test=False):
    print(find_ways(get_input(is_test), 'start', frozenset()))


def find_ways2(graph, start, visited, twice):
    ways = set()
    if start == 'end':
        ways.add(tuple(['end']))
        return ways
    visited = set(visited)
    visited.add(start)
    for v in graph[start]:
        if v not in visited or v.isupper():
            for way in find_ways2(graph, v, frozenset(visited), twice):
                ways.add((start, ) + way)
        elif v in visited and v == twice:
            for way in find_ways2(graph, v, frozenset(visited), ""):
                ways.add((start, ) + way)
    return ways


def part2(is_test=False):
    graph = get_input(is_test)
    ways = set()
    for v in graph:
        if v.isupper() or v == 'start' or v == 'end':
            continue
        for way in find_ways2(graph, 'start', frozenset(), v):
            ways.add(way)
    print(len(ways))


if __name__ == "__main__":
    part1()
    part2()
