import heapq
from pathlib import Path


def valid(idx, collection):
    return 0 <= idx < len(collection)


def valid_in_matrix(i, j, matrix):
    return valid(i, matrix) and valid(j, matrix[i])


def get_input_(idx):
    return open(Path(f'./input/input{idx}.txt'))


class HashHeap(set):
    def __init__(self, iterable):
        super().__init__(iterable)
        self._heap = list(iterable)
        heapq.heapify(self._heap)

    def add(self, element):
        if element not in self:
            heapq.heappush(self._heap, element)
            super().add(element)

    def clear(self):
        super().clear()
        self._heap.clear()

    def copy(self):
        ret = HashHeap(super().copy())
        ret._heap = list(ret)
        heapq.heapify(ret._heap)
        return ret

    def difference(self, *s):
        raise NotImplemented()

    def difference_update(self, *s):
        raise NotImplemented()

    def discard(self, element):
        super().discard(element)

    def intersection(self, *s):
        raise NotImplemented()

    def intersection_update(self, *s):
        raise NotImplemented()

    def isdisjoint(self, s):
        raise NotImplemented()

    def pop(self):
        ret = heapq.heappop(self._heap)
        super().remove(ret)
        return ret

    def remove(self, element):
        raise NotImplemented()

    def symmetric_difference(self, s):
        raise NotImplemented()

    def symmetric_difference_update(self, s):
        raise NotImplemented()

    def union(self, *s):
        ret = HashHeap(super().union(*s))
        ret._heap = list(ret)
        heapq.heapify(ret._heap)
        return ret

    def update(self, *s):
        for e in s:
            if e not in self:
                heapq.heappush(self._heap, e)
        super().update(*s)

    def push(self, element):
        self.add(element)

